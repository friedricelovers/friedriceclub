(function () {
  'use strict';

  /* @ngdoc object
   * @name friedRiceClub
   * @requires $urlRouterProvider
   *
   * @description
   *
   */
  angular
    .module('friedRiceClub', [
      'ngAria',
      'ui.router',
      'ui.bootstrap',
      'home'
    ]);

  angular
    .module('friedRiceClub')
    .config(config);

  function config($urlRouterProvider) {
    $urlRouterProvider.otherwise('/home');
  }

})();
